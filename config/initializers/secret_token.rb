# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
Scavhunt::Application.config.secret_key_base = 'f7de736f6395235ed99fb68617925a49a38fb01f1f172eb8d1a154eb3f48d20dafca9b0463a5af263f110fc6c511e7ef49275e850f69921f403165e30c0de4b1'
