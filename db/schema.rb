# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20141109022345) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "destinations", force: true do |t|
    t.integer  "order"
    t.string   "clue"
    t.string   "value"
    t.boolean  "status"
    t.string   "img_url"
    t.integer  "stage_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "destinations", ["stage_id"], name: "index_destinations_on_stage_id", using: :btree

  create_table "hunts", force: true do |t|
    t.string   "title"
    t.string   "img_url"
    t.string   "url"
    t.string   "password"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "user_id"
  end

  add_index "hunts", ["user_id"], name: "index_hunts_on_user_id", using: :btree

  create_table "stages", force: true do |t|
    t.string   "description"
    t.string   "text"
    t.string   "img_url"
    t.boolean  "status"
    t.integer  "min_points"
    t.integer  "hunt_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "stage_order"
  end

  add_index "stages", ["hunt_id"], name: "index_stages_on_hunt_id", using: :btree

  create_table "tasks", force: true do |t|
    t.text     "text"
    t.boolean  "status"
    t.integer  "points"
    t.integer  "stage_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "tasks", ["stage_id"], name: "index_tasks_on_stage_id", using: :btree

  create_table "users", force: true do |t|
    t.string   "email"
    t.string   "url"
    t.string   "password"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
