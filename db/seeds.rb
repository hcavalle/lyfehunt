# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
#
#



#USER
User.delete_all
user = User.create!(
  email: 'user@user.com', 
  url: 'testuser',
  password: 'password',
)

#HUNT
Hunt.delete_all
hunt = Hunt.create!( 
  title: "Test Hunt",
  url: 'test_hunt_url', 
  password: 'password',
  img_url: 'test_hunt_img'
)
hunt.id = 1
hunt.save

#Stage
Stage.delete_all
stage1 = Stage.create!(
  stage_order: 1,
  description:'First Stage',
  status: false,
  min_points: 10,
  img_url: 'stage_1_img_url',
  hunt_id: hunt.id
)
stage2 = Stage.create!(
  stage_order: 2,
  description:'Second Stage',
  status: false,
  min_points: 11,
  img_url: 'stage_2_img_url',
  hunt_id: hunt.id
)

#Destination
Destination.delete_all
Destination.create!(
  stage_id: stage1.id,
  clue: 'find the thing you sit on',
  value: 'butt',
  status: false,
  img_url: 'dest_1_img_url'
)
  
Destination.create!(
  stage_id: stage2.id,
  clue: 'what is up.',
  value: 'the sky',
  status: false,
  img_url: 'dest_2_img_url'
)

#TASKS
Task.delete_all
task1_s1 = Task.create!(
  stage_id: stage1.id,
  text: "stage_1_task1",
  status: false,
  points: 10,
)
task2_s1 = Task.create!(
  stage_id: stage1.id,
  text: "stage_1_task2",
  status: false,
  points: 11,
)
task3_s1 = Task.create!(
  stage_id: stage1.id,
  text: "stage_1_task3",
  status: false,
  points: 12,
)
task1_s2 = Task.create!(
  stage_id: stage2.id,
  text: "stage_2_task1",
  status: false,
  points: 5,
)
task2_s2 = Task.create!(
  stage_id: stage2.id,
  text: "stage_2_task2",
  status: false,
  points: 6,
)
    
