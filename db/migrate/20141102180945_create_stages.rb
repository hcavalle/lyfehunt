class CreateStages < ActiveRecord::Migration
  def change
    create_table :stages do |t|
      t.string :description
      t.string :text
      t.string :img_url
      t.boolean :status
      t.integer :min_points
      t.belongs_to :hunt, index: true

      t.timestamps
    end
  end
end
