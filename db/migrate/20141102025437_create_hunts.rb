class CreateHunts < ActiveRecord::Migration
  def change
    create_table :hunts do |t|
      t.string :title
      t.string :img_url
      t.string :url
      t.string :password

      t.timestamps
    end
  end
end
