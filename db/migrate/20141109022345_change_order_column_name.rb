class ChangeOrderColumnName < ActiveRecord::Migration
  def change
    rename_column :stages, :order, :stage_order
  end
end
