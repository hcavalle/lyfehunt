class CreateTasks < ActiveRecord::Migration
  def change
    create_table :tasks do |t|
      t.text :text
      t.boolean :status
      t.integer :points
      t.belongs_to :stage, index: true

      t.timestamps
    end
  end
end
