class AddUserRefToHunts < ActiveRecord::Migration
  def change
    add_reference :hunts, :user, index: true
  end
end
