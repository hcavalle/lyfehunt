class CreateDestinations < ActiveRecord::Migration
  def change
    create_table :destinations do |t|
      t.integer :order
      t.string :clue
      t.string :value
      t.boolean :status
      t.string :img_url
      t.belongs_to :stage, index: true

      t.timestamps
    end
  end
end
