class HunterController < ApplicationController

  def index
    @hunts = Hunt.order(:id)
  end
  
  def load_hunt
    @hunt = Hunt.find(params[:id])
    @stage = Stage.where(["hunt_id = ? and status = false", @hunt.id]).order(stage_order: :asc).first
    @destination = Destination.where(["stage_id = ?", @stage.id]).first # (where stage =@stage)
    @all_stages = Stage.where(["hunt_id = ? and stage_order <= ?", @hunt.id, @stage.stage_order])
    @stage_ids = @all_stages.pluck(:id)
    #Load all tasks from current or previous stages & order appropriately
    @tasks = Task.where("stage_id = ? or status = ?",@stage.id, false).order(status: :asc, stage_id: :asc, points: :desc)
    @points_needed = Stage.where("stage_order <= "+@stage.stage_order.to_s).sum(:min_points) 
    @current_points = @tasks.where("status = true").sum(:points)


    session[:current_hunt] = @hunt.id
    session[:current_stage] = @stage.id
    session[:current_destination] = @destination.id
    session[:current_points] = @current_points
    session[:points_needed] = @points_needed

    #@result = check_stage_complete
    
    render(action: :load_hunt)
  end
  
  #pass in destination id
  def check_dest_solve cur_dest = nil
    #checks destination string for correctness
    #updates and moves on 
    dest_message = {}

    destination = Destination.find(cur_dest)
    destination.value.each_char do |v|
      if params["values"][v] == v
        dest_message["message"] = "You got it!"
        dest_message["type"] = :success
        destination.status=true
        destination.save
      else
        dest_message ["message"] = "Sorry, destination you entered is wrong. Try again."
        dest_message["type"] = :error
      end
    end
    return dest_message
    #redirect_to :back
  end
  
  def toggle_tasks
    # flip task status
    params[:tasks].each do |key, value|
      @t = Task.find(key.to_i)
      @t.update(status: value)
    end
  end

  def submit_stage
    #check if stage is complete by calc current point total & points needed
    @params = params 
    @destination = Destination.find(session[:current_destination])

    #check_dest_solve 
    if !@destination.status
      dest_message= check_dest_solve @destination.id
      flash[dest_message["type"]] = dest_message["message"]
    end

    #check tasks
    toggle_tasks

    @stage = Stage.find session[:current_stage]
    complete = @stage.check_complete
    if complete
      @hunt = Hunt.find(session[:current_hunt])
    else
      redirect_to :back
    end
  end

  #prob won't be used
  def lauren
    @hunt = Hunt.find(1)
    #@stages = Stage.order(:id)
    #use link_to or ..._path instead to get stages
      # first need to implement 'references' and has_many etc correctly, pg 109
    first_stage = @stages.find(1)
  end
end
