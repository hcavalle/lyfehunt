json.array!(@tasks) do |task|
  json.extract! task, :id, :text, :status, :points, :stage_id
  json.url task_url(task, format: :json)
end
