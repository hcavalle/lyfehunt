json.array!(@destinations) do |destination|
  json.extract! destination, :id, :order, :clue, :value, :status, :img_url, :stage_id
  json.url destination_url(destination, format: :json)
end
