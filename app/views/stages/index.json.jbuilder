json.array!(@stages) do |stage|
  json.extract! stage, :id, :description, :text, :img_url, :status, :min_points, :hunt_id
  json.url stage_url(stage, format: :json)
end
