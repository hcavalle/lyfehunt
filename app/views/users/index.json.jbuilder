json.array!(@users) do |user|
  json.extract! user, :id, :email, :url, :password
  json.url user_url(user, format: :json)
end
