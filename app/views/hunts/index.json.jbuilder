json.array!(@hunts) do |hunt|
  json.extract! hunt, :id, :title, :img_url, :url, :password
  json.url hunt_url(hunt, format: :json)
end
