class Task < ActiveRecord::Base
  belongs_to :stage

  def self.load_task_by_stage stage_id
    self.where("stage_id="+stage_id.to_s)
  end
end
