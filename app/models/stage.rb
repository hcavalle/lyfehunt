class Stage < ActiveRecord::Base
  belongs_to :hunt
  has_many :tasks
  has_one :destination

  accepts_nested_attributes_for :destination
  accepts_nested_attributes_for :tasks

  scope :full, -> {joins(:destination)}

  def full
    self.joins(:destination, :tasks)
  end

  def get_dests
    self.joins(:destination)
  end
  def check_complete
    #TODO: refactor to check total points vs total accumulated
    points = Task.where("status = true").sum(:points)
    min_points = Stage.where("stage_order <= "+self.stage_order.to_s).sum(:min_points) 

    if self.destination.status && points >= min_points
      self.status = true
      self.save
      true
    else
      false
    end
  end
end
